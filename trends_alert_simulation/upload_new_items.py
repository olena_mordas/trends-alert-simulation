from squirro_client import ItemUploader
import datetime
import time

processing_config = {'filtering': {'enabled': True}}

number_of_items = 30
sleep_time = 3  # seconds
item_counter = 1

# uploader
uploader = ItemUploader(
    client_id=None, client_secret=None, 
    config_file='config.ini')

for item in xrange(number_of_items):

    # prepare and uploade item
    creation_time = datetime.datetime.now()
    creation_time_str = datetime.datetime.strftime(
        creation_time, '%Y-%m-%dT%H:%M:%S')

    # not a typical bug-tracker item, just an artificial item to simulate the
    # online processing of the trends tutorial
    items = [{'created_at': creation_time_str, 'title': creation_time_str, 'body': '<p>ncmsjnckjdsncdsvcksndvnsl</p>', }]
    uploader.upload(items)

    # feedback
    print 'uploaded {0} items, {1} more to go'.format(
        item_counter, number_of_items - item_counter)
    item_counter += 1
    time.sleep(sleep_time)
